import { Injectable }     from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { ProductType } from '../model/product-type';
import { Observable }     from 'rxjs/Observable';


@Injectable()
export class ProductTypeService {
    
    private url = '/webshop2/productTypes'; 
  
    constructor (private http: Http) {} 
  
    getProducts(): Observable<ProductType[]> {
        return this.http.get(this.url)
                    .map(this.extractData)
                    .catch(this.handleError); 
    }
    
    private extractData(res: Response) {
        return res.json() || {};
    }
    
    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}