import { Component, OnInit } from '@angular/core';

import { Product } from '../model/product';
import { ProductService } from '../service/product.service';

import { ActivatedRoute, Params } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'app-home',
    templateUrl: 'product.component.html' 
})
export class ProductComponent implements OnInit { 
    
    products: Product[];
    selectedProduct: Product;
    errorMessage: string;
    
     constructor (
        private productService: ProductService,
        private route: ActivatedRoute) {
    }
    
    ngOnInit(): void {
        this.route.params
          .switchMap((params: Params) => this.productService.getProductsByProductType(params['productType']))
          .subscribe(
                  data => {
                    this.products = data;
                    console.dir(data);
                  },
                  error =>  this.errorMessage = error); 

    }
    
    getProduct (id: number) {
        this.productService.getProduct(id)
            .subscribe(
                  data => this.selectedProduct = data,
                  error =>  this.errorMessage = error); 
    }
    
    getProducts () {
        this.productService.getProducts()
            .subscribe(
                  data => {
                    this.products = data;
                    console.dir(data);
                  },
                  error =>  this.errorMessage = error); 
    }
    
    getProductsByProductType(productType: number) {
    	this.productService.getProductsByProductType(productType)
            .subscribe(
                  data => {
                    this.products = data;
                    console.dir(data);
                  },
                  error =>  this.errorMessage = error); 
    }
    
    isAvailable(product: Product) {
        return product.quantity > 0 ? 'Available' : 'Out of stock';
    }
    
}