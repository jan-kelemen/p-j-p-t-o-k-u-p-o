import { Component, OnInit } from '@angular/core';

import { ProductType } from '../model/product-type';
import { ProductTypeService } from '../service/product-type.service';

@Component({
	moduleId: module.id,
	selector: 'app-sidenav',
	templateUrl: 'sidenav.component.html'
})
export class SidenavComponent implements OnInit {

    productTypes: ProductType[];
    errorMessage: string;
    
     constructor (
        private productTypeService: ProductTypeService) {
    }
    
    ngOnInit() { 
        this.getProductTypes();
    }
    
    getProductTypes() {
        this.productTypeService.getProducts()
            .subscribe(
                  data => {
                    this.productTypes = data;
                    console.dir(data);
                  },
                  error =>  this.errorMessage = error); 
    }
    
}