package hr.fer.webshop2.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

public class JpaService<T> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected JpaRepository<T, Long> repository;

    public List<T> findAll() {
        logger.debug("JpaService.findAll(); init");

        final List<T> rv = repository.findAll();
        final int count = rv == null ? 0 : rv.size();

        logger.debug("Found {} rows.", count);
        return rv;
    }

    public T findOne(final Long id) {
        return repository.findOne(id);
    }

    public T save(final T entity) {
        return repository.save(entity);
    }

    public void delete(final T entity) {
        repository.delete(entity);
    }
}
