package hr.fer.webshop2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.fer.webshop2.domain.entity.Order;
import hr.fer.webshop2.domain.entity.Product;
import hr.fer.webshop2.domain.repository.ProductRepository;

@Service
@Transactional
public class OrderService extends JpaService<Order> {

    @Autowired
    protected ProductRepository productRepository;

    public Order addProduct(final Long id, final Order order) {
        final List<Order> orders = repository.findAll();
        Order rv = orders.isEmpty() ? order : orders.get(0);

        final Product product = productRepository.findOne(id);
        if (product != null) {
            rv.getProducts().add(product);
            rv = repository.save(rv);
        }

        return rv;
    }
}
