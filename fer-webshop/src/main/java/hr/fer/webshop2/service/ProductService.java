package hr.fer.webshop2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hr.fer.webshop2.domain.entity.Product;
import hr.fer.webshop2.domain.entity.ProductType;
import hr.fer.webshop2.domain.repository.ProductRepository;

@Service
@Transactional
public class ProductService extends JpaService<Product> {

    @Autowired
    private JpaRepository<ProductType, Long> productTypeRepository;

    public List<Product> getProductsByProductType(final Long productTypeId) {
        final ProductType productType = productTypeRepository.findOne(productTypeId);
        return ((ProductRepository) repository).findByProductType(productType);
    }
}
