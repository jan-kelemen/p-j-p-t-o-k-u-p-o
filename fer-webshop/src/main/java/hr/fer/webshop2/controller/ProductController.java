package hr.fer.webshop2.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.webshop2.domain.entity.Product;
import hr.fer.webshop2.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController extends JpaRestController<Product> {

    @RequestMapping(value = "/type/{productType}", method = RequestMethod.GET)
    public List<Product> getProductsByProductType(@PathVariable final Long productType) {
        final List<Product> rv = ((ProductService) service).getProductsByProductType(productType);
        return rv;
    }
}
