package hr.fer.webshop2.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.webshop2.domain.entity.Product;
import hr.fer.webshop2.domain.entity.ProductType;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    public List<Product> findByProductType(ProductType productType);
}
